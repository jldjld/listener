<?php /**
 * @global $secret_key
 * @global $regenerate_key_url
 * @global $main_site_data
 * @global $main_plugins_data
 * @global $activated_plugins
 * @global $plugins_updates
 */
?>
<h1>Sevanova Listener</h1>
<h2>Your secret key</h2>
<p><?php echo esc_attr( __( $secret_key ) ); ?></p>
<p><a href="<?php echo esc_url( $regenerate_key_url ); ?>">Generate a new secret key</a></p>

<h3> Domain </h3>
<form name="set_domain" id="set_domain" action="#" method="POST" autocomplete="off">
    <label for="my_domain" class="set_domain_label">Set your domain</label>
    <input type="text" id="my_domain" class="my_domain" placeholder="Your authorized domain" value=""/>
    <input type="submit" name="wp-submit" id="wp-submit" class="set_domain_btn" value="Set this domain"/>
</form>

<h4>Project Main informations</h4>
<p>NAME : <?php echo esc_html( $main_site_data['main_infos']['label'] ); ?> </p>
<p>URL : <?php echo esc_url( get_rest_url() ); ?> </p>
<p>LOGIN URL : <?php echo esc_url( $main_site_data['main_infos']['login_url'] ); ?> </p>
<p>DESCRIPTION : <?php echo esc_html( $main_site_data['main_infos']['description'] ) ?> </p>
<p>WP VERSION : <?php echo esc_html( $main_site_data['main_infos']['wp_version'] ); ?> </p>
<p>MULTISITE : <?php echo esc_html( $main_site_data['main_infos']['is_multisite'] ); ?> </p>
<p>LIST OF PLUGINS : <?php dump ( $main_plugins_data ); ?> </p>
<p>ACTIVATED PLUGINS : <?php dump ( $activated_plugins ); ?> </p>
<p>PLUGINS UPDATES AVAILABLE : <?php dump ( $plugins_updates ); ?> </p>
