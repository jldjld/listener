# PLUGIN RÉALISÉ DURANT MON ANNÉE D'ALTERNANCE /

## DESCRIPTION 

Outil de monitoring WP, pour surveiller l’état de santé, des projets WordPress d'une agence.

Le plugin communique avec un tableau de bords, sur lequel les informations clés de projets en ligne,
sont régulièrement mises à jour à l'aide d'un CRON.

Fonctionnalité implémentées:

- Initialisation d'une route custom REST API
- Récupération d'informations clés sur un projet WP
- Création d'un sous-menu Listener, dans le menu outils

Le Thème développé pour ce projet est un dépot privé.

https://gitlab.com/jldjld/wp-dashboard

