<?php
namespace SN\Sevanova_Listener\Admin;

use SN\Sevanova_Listener\Helpers;

class Main {

	/**
	 * Use the trait
	 */
	use \SN\Sevanova_Listener\Singleton;

	protected function init() {
		add_action( 'admin_init', [ $this, 'check_key_update' ] );
		add_action( 'admin_enqueue_scripts', [ $this, 'enqueue_styles' ] );
		add_action( 'admin_enqueue_scripts', [ $this, 'enqueue_scripts' ] );
		add_action( 'admin_menu', [ $this, 'register_listener_sub_menu' ] );
        add_shortcode("screenshot", [ $this, "get_screenshot" ] );
    }

	/**
	 * Check for secret key update
	 */
	public function check_key_update() {
		$action = filter_input( INPUT_GET, 'action', FILTER_DEFAULT );
		if ( 'seva-flush-key' !== $action ) {
			return;
		}

		check_admin_referer( 'seva-flush-key-action' );

		if ( ! Helpers::register_secret_key() ) {
			wp_die( 'Error while generating new key' );
		}

		wp_safe_redirect( admin_url( '/tools.php?page=sevanova_listener&success=true' ) );
		exit;
	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {
		wp_enqueue_style( 'sevanova-listener-admin', SEVANOVA_LISTENER_URL . 'assets/admin/css/sevanova-listener-admin.css', array(), SEVANOVA_LISTENER_VERSION, 'all' );
	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {
		wp_enqueue_script( 'sevanova-listener-admin', SEVANOVA_LISTENER_URL . 'assets/admin/js/sevanova-listener-admin.js', array( 'jquery' ), SEVANOVA_LISTENER_VERSION, false );
	}


	/**
	 * Adding Listener sub_menu_page
	 * @return false|string
	 */
	public function register_listener_sub_menu() {
		if ( ! current_user_can( 'administrator' ) ) {
			return false;
		}
		return add_management_page( 'Sevanova Listener', 'Listener', 'administrator', 'sevanova_listener', [ $this, 'listener_page_content' ], 3 );
	}

	/**
	 * Display listener page content
	 */
	public function listener_page_content() {
		$secret_key = \SN\Sevanova_Listener\Helpers::get_secret_key();
		if ( empty( $secret_key ) || ! is_string( $secret_key ) ) {
			if ( ! \SN\Sevanova_Listener\Helpers::register_secret_key() ) {
				wp_die( 'Cannot generate secret key' );
			}

			$secret_key = \SN\Sevanova_Listener\Helpers::get_secret_key();
			if ( empty( $secret_key ) || ! is_string( $secret_key ) ) {
				wp_die( 'Cannot generate secret key (2)' );
			}
		}

		// Get site data
		$main_site_data = \SN\Sevanova_Listener\Helpers::get_main_info();
		if ( ! is_array( $main_site_data ) || empty( $main_site_data ) ) {
			wp_die( 'Error while getting site data' );
		}

		$main_plugins_data = \SN\Sevanova_Listener\Helpers::get_plugins_main_info();
		if ( ! is_array( $main_plugins_data ) || empty( $main_plugins_data ) ) {
			wp_die( 'Error while getting plugins data' );
		}

		$activated_plugins = \SN\Sevanova_Listener\Helpers::get_active_plugins();
		if ( ! is_array( $activated_plugins ) || empty( $activated_plugins ) ) {
			wp_die( 'Error while getting activated plugins data' );
		}

		$plugins_updates = \SN\Sevanova_Listener\Helpers::get_plugins_updates_available();
		if ( ! is_array( $plugins_updates ) || empty( $plugins_updates ) ) {
			wp_die( 'Error while getting available plugins updates' );
		}

		$regenerate_key_url = wp_nonce_url( add_query_arg(
			[
				'action' => 'seva-flush-key'
			],
			admin_url()
		), 'seva-flush-key-action' );

		require_once( SEVANOVA_LISTENER_DIR . 'views/admin-listener-site-data.php' );
	}
}
