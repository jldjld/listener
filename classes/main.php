<?php
namespace SN\Sevanova_Listener;

class Main {
	/**
	 * Use the trait
	 */
	use Singleton;

	/**
	 * The private singleton constructor
	 *
	 * @param void
	 * @return void
	 */
	protected function init() {
		add_action( 'plugins_loaded', [ $this, 'load_plugin_textdomain' ] );
	}

	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {
		load_plugin_textdomain(
			'sevanova-listener',
			false,
			SEVANOVA_LISTENER_DIR . '/languages/'
		);
	}
}
