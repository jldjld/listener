<?php
namespace SN\Sevanova_Listener;

class Helpers {

	/**
	 * Get basic wp app informations
	 * @return array|void
	 */
	public static function get_main_info() {
		$main_info = [
			'label'        => sanitize_title( get_bloginfo( 'name' ) ),
			'url'          => get_bloginfo( 'url' ),
			'login_url'    => wp_login_url(),
			'description'  => get_bloginfo( 'description' ),
			'wp_version'   => get_bloginfo( 'version' ),
			'is_multisite' => is_multisite(),
            'screenshot'   => self::get_screenshot(),

        ];

		if ( empty( $main_info ) ) {
			return;
		}

		return array_merge(
			[
				'main_infos'                => $main_info,
                //'health'					=> self::get_project_health_check(),
				'plugins'                   => self::get_plugins_main_info(),
				'active_plugins'            => self::get_active_plugins(),
				'plugins_updates_available' => self::get_plugins_updates_available(),
			]
		);
	}

	/**
	 * Get WP project health infos
	 * @return array|false
	 */
	private static function get_project_health_check() {

		$health = \WP_Site_Health::get_instance();
		if ( empty( $health ) ) {
			return false;
		}
		// TODO : piste pour appel des méthodes depuis le plugin
		if ( ! function_exists( 'get_plugin_updates' ) ) {
			require ABSPATH . WPINC .'/update.php';
		}
		// Get general health check status
		$health_data = [
			'plugins'     => $health->get_test_plugin_version(),
			'sql_server'  => $health->get_test_sql_server(),
			'php_version' => $health->get_test_php_version(),
			'wp_version'  => $health->get_test_wordpress_version(),
			'ssl_support' => $health->get_test_ssl_support(),
		];

		foreach ( $health_data as $key => $datum ) {
			if ( empty( $datum ) ) {
				continue;
			}

			$health_data[ $key ] = [
				'status' => $datum['status'],
				'label'  => $datum['label'],
			];
		}

		return $health_data;
	}

    /**
     * Get theme screenshot
     * @return mixed
     */
	public static function get_screenshot() {
        $theme = new \WP_Theme( wp_get_theme()->stylesheet, wp_get_theme()->theme_root );
        return $theme->get_screenshot();
    }

	/**
	 * Get plugins array
	 * @return array|array[]|\WP_Error
	 */
	private static function get_all_plugins() {
		$plugins = get_plugins();
		if ( empty( $plugins ) || ! is_array( $plugins ) ) {
			return new \WP_Error( 'no_plugins_found', esc_html__( 'No plugins found on this project' ) );
		}

		return $plugins;
	}

	/**
	 * Get custom array of plugins data
	 * @return array
	 */
	public static function get_plugins_main_info() {
		$plugins_data = [];
		// Get project plugins list
		$plugins = self::get_all_plugins();
		foreach ( $plugins as $key => $plugin ) {
			$plugins_data[] = [
				'name'                 => $plugin['Name'],
				'version'              => $plugin['Version'],
				'activated'            => is_plugin_active( $key ),
				'description'          => $plugin['Description'],
				'required WP version'  => $plugin['RequiresWP'],
				'required PHP version' => $plugin['RequiresPHP'],
				'key'                  => $key,
				'url'                  => $plugin['PluginURI'],
			];
		}

		return $plugins_data;
	}

	/**
	 * Display active plugins array
	 * @return array
	 */
	public static function get_active_plugins() {
		$activated_plugins = [];
		// Get plugins activated status
		$plugins = self::get_plugins_main_info();
		foreach ( $plugins as $plugin ) {
			if ( false !== $plugin['activated'] ) {
				$activated_plugins[] = $plugin;
			}
		}

		return $activated_plugins;
	}

	/**
	 * Get list of plugins available updates
	 * @return false|mixed
	 */
	public static function get_plugins_updates_available() {
		$plugins_transient = get_site_transient( 'update_plugins' );
		if ( empty( $plugins_transient->response ) ) {
			return false;
		}

		$updates = [];
		foreach ( $plugins_transient->response as $key => $response ) {
			$updates[] = [
				'name'        => $key,
				'version' => $response->new_version,
				'url'         => $response->url,
			];
		}

		return $updates;
	}

	/**
	 * Get the current secret key
	 *
	 * @return string|false
	 */
	public static function get_secret_key() {
		return get_option( 'secret_key' );
	}

	/**
	 * Hash secret key
	 * @return string
	 */
	private static function hash_secret_key() {
        // Secret key salt
        $salt = wp_rand( 89, 1030 );
        // Generate random string
        $rand_str = str_shuffle( 'AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz' );
		// Hashing password
		return hash( 'sha512', $rand_str, false ) . $salt . getenv( 'DASH_PEPPER' );
	}

	/**
	 * Register secret key, if it does not exists
	 * @return bool
	 */
	public static function register_secret_key() {
		$secret_key = self::hash_secret_key();
		return update_option( 'secret_key', $secret_key );
	}
}

