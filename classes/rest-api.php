<?php
namespace SN\Sevanova_Listener;

class Rest_API {

	/**
	 * Use the trait
	 */
	use \SN\Sevanova_Listener\Singleton;

	protected function init() {
		add_action( 'rest_api_init', [ $this, 'register_project_route' ] );
		add_filter( 'rest_url_prefix', [ $this, 'custom_prefix' ] );
	}

	/**
	 * Removing default wp-json rest prefix
	 * @return string
	 */
	public function custom_prefix() {
		return 'seva';
	}

	/**
	 * Register custom rest api route with secret key
	 * @return bool
	 */
	public function register_project_route() {

		$secret_key = Helpers::get_secret_key();
		if ( empty( $secret_key ) ) {
			return false;
		}

		return register_rest_route(
			'json',
			'/site-data',
			[
				'methods'  => \WP_REST_Server::READABLE,
				'callback' => [ $this, 'get_project_json' ],
			]
		);
	}

	/**
	 * Display rest route content if header token is valid
	 * @return string
	 */
	public function get_project_json() {

		// Get secret key
		$secret_key = Helpers::get_secret_key();
		if ( empty( $secret_key ) ) {
			return false;
		}
		// Get sent auth header
		$headers = getallheaders();
		$header  = $headers['api_key'];

		if ( $header !== $secret_key ) {
			wp_die( 'You do not have access to this ressource' );
		}
		$project_main_infos = Helpers::get_main_info();

		$response = new \WP_REST_Response();

		if ( empty( $project_main_infos ) ) {
			$response->set_status( 404 );
			return $response->get_status();
		}
		// Set data to wp response
		$response->set_data( $project_main_infos );
		rest_ensure_response( $response );

		return $response->get_data();
	}
}
