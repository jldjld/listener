<?php
/**
 * @wordpress-plugin
 * Plugin Name:       Sevanova Listener
 * Plugin URI:        http://example.com/sevanova-listener-uri/
 * Description:       Checks WordPress applications global health status, to avoid security and efficiency loss.
 * Version:           1.0.0
 * Boilerplate:       2.0.2
 * Author:            Sevanova
 * Author URI:        http://example.com/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       sevanova-listener
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'SEVANOVA_LISTENER_VERSION', '1.0.0' );
define( 'SEVANOVA_LISTENER_DIR', plugin_dir_path( __FILE__ ) );
define( 'SEVANOVA_LISTENER_URL', plugin_dir_url( __FILE__ ) );
define( 'SEVANOVA_LISTENER_PLUGIN_DIRNAME', basename( rtrim( dirname( __FILE__ ), '/' ) ) );

require_once SEVANOVA_LISTENER_DIR . 'autoload.php';

// Plugin activate/deactive hooks
register_activation_hook( __FILE__, array( '\SN\Sevanova_Listener\Plugin', 'activate' ) );
register_deactivation_hook( __FILE__, array( '\SN\Sevanova_Listener\Plugin', 'deactivate' ) );

add_action( 'plugins_loaded', 'run_sevanova_listener' );
function run_sevanova_listener() {
	\SN\Sevanova_Listener\Main::get_instance();
	\SN\Sevanova_Listener\Main_Informations::get_instance();
	\SN\Sevanova_Listener\Rest_API::get_instance();

	if ( is_admin() ) {
		\SN\Sevanova_Listener\Admin\Main::get_instance();
	}
}
